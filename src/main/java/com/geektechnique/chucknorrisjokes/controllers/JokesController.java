package com.geektechnique.chucknorrisjokes.controllers;

import com.geektechnique.chucknorrisjokes.services.JokesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class JokesController {

    private JokesService jokesService;

    @Autowired
    public JokesController(JokesService jokesService){
        this.jokesService = jokesService;
    }

    @RequestMapping({"/",""})//don't really get how this is dif from "/"
    public String printJoke(Model model){

        model.addAttribute("joke", jokesService.getJoke());

        return "chucknorris1";//must be the thymleaf template name
    }
}
